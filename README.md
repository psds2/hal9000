# hal9000

Material created by team Hal9000 as part of the Dell Technologies AI hackathon.

exploratory_analysis.ipynb contains the code to generate data exploration visualizations such as label distribution, word clouds and dataset stats.

Model_1_sepcnn.ipynb contains the code for data cleaning, feature engineering, model training and inference.