Instructions to run tfserving container

First, pull the tfserving container from DockerHub
```
docker pull tensorflow/serving
```

Clone the tfserving github repo
```
git clone https://github.com/tensorflow/serving
```

Copy your model file to the models directory and set the location of the saved model file
```
MODELSPOT="$(pwd)/models"
```

Start the tfserving container. Be sure to replace *yourmodelname* with the actual file name of your saved model file
```
docker run -t --rm -p 8501:8501 \
    -v "$MODELSPOT:/models/" \
    -e MODEL_NAME=yourmodelname \
    tensorflow/serving &
```



